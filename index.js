const USERS_URL = "https://ajax.test-danit.com/api/json/users";
const POSTS_URL = "https://ajax.test-danit.com/api/json/posts";

const root = document.getElementById('root');
const animation = document.getElementById('animation');

class Card {
    request(url) {
        return fetch(url).then(function (res) {
            return res.json();
        });
    }

    getDataFromUrl(url) {
        return this.request(url);
    }

    createPostHTML(post, user) {
        let div = document.createElement('div');
        div.classList.add('card');
        div.id = 'post-' + post.id;
        let postDiv = document.createElement('div');
        postDiv.classList.add('post');

        let title = document.createElement('h4');
        title.textContent = post.title;

        let body = document.createElement('p');
        body.textContent = post.body;

        let deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete this post';
        deleteButton.setAttribute('data-post-id', post.id);

        deleteButton.addEventListener('click', this.removePost);

        postDiv.append(title, body, deleteButton);

        let userDiv = document.createElement('div');
        userDiv.classList.add('user');

        let userTitle = document.createElement('p');
        userTitle.append('USER: #' + user.id, document.createElement('br'), user.name);

        let email = document.createElement('p');
        email.textContent = "Email: " + user.email;

        userDiv.append(userTitle, email);

        div.append(userDiv, postDiv);
        return div;
    }
    renderPosts(posts, users) {
        return posts.map((post) => {
            return this.createPostHTML(post, users[post.userId]);
        });
    }

    removePost(e) {
        const postId = e.target.getAttribute('data-post-id');
        if (postId) {
            fetch("https://ajax.test-danit.com/api/json/posts/" + postId, {
               method: "DELETE"
            })
                .then((data) => {
                    const post = document.getElementById('post-' + postId)?.remove();
                })
                .catch(err => console.log(err));
        }
    }
}

let card = new Card();

card.getDataFromUrl(USERS_URL).then((users) => {
    let userItems = {};
    for (let user of users) {
        userItems[user.id] = user;
    }

    card.getDataFromUrl(POSTS_URL).then((posts) => {
        animation.style.display = 'none';
        root.append(...card.renderPosts(posts, userItems));
    });
});
